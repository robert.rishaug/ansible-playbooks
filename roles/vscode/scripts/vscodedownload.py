import sys
import requests
import json

OS_KEY = "linux-deb-x64"

r = requests.get('https://code.visualstudio.com/sha?build=stable')
json = r.json()

for product in json['products']:
    if product['platform']['os'] == OS_KEY:
        os_specific_product_url = product['url']
        os_specific_product_sha256 = product['sha256hash']

if sys.argv[1] == "url":
    print(os_specific_product_url, end='')
elif sys.argv[1] == "sha256":
    print(os_specific_product_sha256, end='')
else:
    print(f"Unknown arg {sys.argv[1]}", end='')
    sys.exit(1)
