#!/bin/bash
set -eu pipefail

sudo apt-get -y install git python3.9 python3.9-venv
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
 